package com.app.myappjhsch.general.ws;
import com.app.myappjhsch.general.bean.Codigo;
import retrofit2.Call;
import retrofit2.http.GET;
public interface CodigoServicio {
    @GET("code")
    Call<Codigo> getCode();
}
