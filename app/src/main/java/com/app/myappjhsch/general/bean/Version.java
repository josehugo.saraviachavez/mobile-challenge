package com.app.myappjhsch.general.bean;
public class Version {
    private String version;
    public String getVersion() {
        return version;
    }
    @Override
    public String toString() {
        return "Version{" +
                "version='" + version + '\'' +
                '}';
    }
}
