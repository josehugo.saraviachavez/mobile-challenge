package com.app.myappjhsch.general.ws;
import com.app.myappjhsch.general.bean.Version;
import retrofit2.Call;
import retrofit2.http.GET;
public interface VersionServicio {
    @GET("version")
    Call<Version> getVersion();
}
