package com.app.myappjhsch.general.ws;
import com.app.myappjhsch.general.bean.Respuesta;
import java.util.HashMap;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
public interface CoordenadaServicio {
    @POST("location")
    Call<Respuesta> postCoordenada(@Body HashMap<String, String> parametros);
}
