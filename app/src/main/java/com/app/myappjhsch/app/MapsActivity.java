package com.app.myappjhsch.app;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.app.myappjhsch.R;
import com.app.myappjhsch.general.bean.Codigo;
import com.app.myappjhsch.general.bean.Coordenada;
import com.app.myappjhsch.general.bean.Respuesta;
import com.app.myappjhsch.general.bean.Version;
import com.app.myappjhsch.general.ws.CodigoServicio;
import com.app.myappjhsch.general.ws.CoordenadaServicio;
import com.app.myappjhsch.general.ws.VersionServicio;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.HashMap;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private Button btnVersion, btnCodigo;
    private GoogleMap mMap;
    private Marker marcador;
    private String sUrlWS;
    private String versionApp;
    private LocationManager locationManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        sUrlWS = getString(R.string.urlWS);
        obtenerVersion();
        btnVersion = (Button) findViewById(R.id.btnVersion);
        btnCodigo = (Button) findViewById(R.id.btnCodigo);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        btnVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mensaje("VersiÃ³n del App: " + versionApp);
            }
        });
        btnCodigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarCodigo();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.i("PERMISO", "LA APP TIENE PERMISO");
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListenerGPS);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION}, 1337);
            }
        }
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void mensaje(String mensaje) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
        builder.setMessage(mensaje)
                .setCancelable(false)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void obtenerVersion() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(sUrlWS).addConverterFactory(GsonConverterFactory.create()).build();
        VersionServicio servicio = retrofit.create(VersionServicio.class);
        Call<Version> version = servicio.getVersion();
        version.enqueue(new Callback<Version>() {
            @Override
            public void onResponse(Call<Version> call, Response<Version> response) {
                versionApp = response.body().getVersion();
            }

            @Override
            public void onFailure(Call<Version> call, Throwable t) {
                Log.d("WS", call.toString());
            }
        });
    }

    private void mostrarCodigo() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(sUrlWS).addConverterFactory(GsonConverterFactory.create()).build();
        CodigoServicio servicio = retrofit.create(CodigoServicio.class);
        Call<Codigo> codigo = servicio.getCode();
        codigo.enqueue(new Callback<Codigo>() {
            @Override
            public void onResponse(Call<Codigo> call, Response<Codigo> response) {
                mensaje("CÃ³digo Generado: " + response.body().getCode());
            }

            @Override
            public void onFailure(Call<Codigo> call, Throwable t) {
                Log.d("WS", call.toString());
            }
        });
    }

    private void enviarGPS(LatLng gps) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(sUrlWS).addConverterFactory(GsonConverterFactory.create()).build();
        CoordenadaServicio servicio = retrofit.create(CoordenadaServicio.class);
        HashMap<String, String> parametros = new HashMap<>();
        Coordenada coordenada = new Coordenada();
        coordenada.setLatitud(gps.latitude);
        coordenada.setLongitud(gps.longitude);
        Gson gson = new GsonBuilder().create();
        parametros.put("json", gson.toJson(coordenada));
        Call<Respuesta> respuesta = servicio.postCoordenada(parametros);
        respuesta.enqueue(new Callback<Respuesta>() {
            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                Log.d("WS", response.body().getStatus());
            }
            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                Log.d("WS", call.toString());
            }
        });
    }

    private void mostrarUbicacion(LatLng coordenadas) {
        if (coordenadas == null) {
            return;
        }
        if (marcador != null) {
            marcador.remove();
        }
        CameraUpdate miUbicacion = CameraUpdateFactory.newLatLngZoom(coordenadas, 18);
        marcador = mMap.addMarker(
                new MarkerOptions()
                        .position(coordenadas)
                        .title("Mi PosiciÃ³n en Vivo")
                        .flat(true)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_huella_01)));
        mMap.animateCamera(miUbicacion);
        enviarGPS(coordenadas);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng gps = new LatLng(-12.116419, -76.976983);
        mostrarUbicacion(gps);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1337:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("PERMISO", "SI SE ACEPTARON PERMISOS");
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListenerGPS);
                    }
                }else{
                    Log.d("PERMISO", "NO SE ACEPTARON PERMISOS");
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private final LocationListener locationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Double latitud = location.getLatitude();
            Double longitud = location.getLongitude();
            final LatLng gps = new LatLng(latitud, longitud);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mostrarUbicacion(gps);
                }
            });
        }
        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {}
        @Override
        public void onProviderEnabled(String s) {}
        @Override
        public void onProviderDisabled(String s) {}
    };
}
