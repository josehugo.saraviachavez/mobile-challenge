pasos:

1) forkea el repo

2) hacer una apk android con los sgtes requisitos:

- UI : un mapa y 2 botones

- boton `version` , al clickar debo ver la version de la aplicacion (Backend) , la version la obtienes de aca: http://159.65.235.115:4567/version, solo deberia cargarse 1 sola vez

- boton `codigo`, al clickar debo obtener un codigo random desde el sgte endpoint: http://159.65.235.115:4567/code, la invocacion se hace cada vez que se le da click al boton

- La pantalla principal donde se muestre un mapa con mi ubicacion en vivo, enviando las coordenadas mediante POST a http://159.65.235.115:4567/location cada 1 minuto. (bonus point: si cuando la aplicacion no esta corriendo sigue enviando coords cada 1 min)

3) documentar en el repo, los pasos del build (deben ser lo suficientemente claros) (bonus point: si usan CD dentro de gitlab para el build)

4) enviar el repo a plataforma@geosatelital.com

si todo sale bien, te invitaremos a una entrevista sea por skype/telefono/oficina para continuar el proceso, suerte!